//
//  VideoExportModel.swift
//  AV enhance
//
//  Created by Apple on 1/5/20.
//  Copyright © 2020 umer. All rights reserved.
//

import UIKit
import RealmSwift

class VideoExportModel: Object {
    
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    @objc dynamic var size = 0.0
    @objc dynamic var duration = 0.0
    @objc dynamic var filePath = ""
    @objc dynamic var fileType = ""
    @objc dynamic var soundLevel = ""
    @objc dynamic var note = ""
    @objc dynamic var dateCreated = Date()
    
    @objc dynamic var fullName = ""

    
    
    override static func primaryKey() -> String? {
        return "id"
    }

}
