//
//  BassConverterViewController.swift
//  AV enhance
//
//  Created by huda elhady on 08/03/2021.
//  Copyright © 2021 umer. All rights reserved.
//

import UIKit
import MediaPlayer
import AVFoundation
import SVProgressHUD
import mobileffmpeg


class BassConverterViewController: UIViewController {
    
    // IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var convertButton: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    
    
    
    @IBOutlet weak var loadingView: UIView!

    // Class Variables
    
    var selectedMediaType = MediaType.Audio
    var mediaItemsArray = [MediaItems]()
    var selectedMediaItem = MediaItems()
    var convertButtonShouldBeEnabled = false
    
    var ffmpegCommandsArray = NSMutableArray()
    var destinationFilePathsArray = NSMutableArray()
    
    var defaultAudioVolume = "0 %  (No Change)"
    var defaultVideoFormat = "MOV"
    var defaultAudioFormat = "AAC"
    weak var delegate: ConverterVCDelegate?
    
    
    var stabilizeVideo = false
    var musicURL: URL?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.setupUIViews()
        self.setDefaultValues()
        
        SVProgressHUD.setDefaultMaskType(.clear)
        self.loadingView.isHidden = true

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        if let tabbar = self.tabBarController {
            CommonClass.sharedInstance.setTabBarTitles(tabbarVC: tabbar)
        }
        
    }
    
    func setDefaultValues() {
        
        // Default Audio Volume
        if let defaultAudioVolume = UserDefaults.standard.value(forKey: Constants.UserDefaults.k_Default_Audio_Volume) as? String {
            self.defaultAudioVolume = defaultAudioVolume
        }
        
        // Default Video Format
        if let defaultVideoFormat = UserDefaults.standard.value(forKey: Constants.UserDefaults.k_Default_Video_Format) as? String {
            self.defaultVideoFormat = defaultVideoFormat
        }
        
        // Default Audio Format
        if let defaultAudioFormat = UserDefaults.standard.value(forKey: Constants.UserDefaults.k_Default_Audio_Format) as? String {
            self.defaultAudioFormat = defaultAudioFormat
        }
        
        for mediaItem in self.mediaItemsArray {
            
            if(mediaItem.isAudio) {
                // Audio
                mediaItem.fileTypeToConvert = self.defaultAudioFormat
            }
            else {
                // Video
                mediaItem.fileTypeToConvert = self.defaultVideoFormat
            }
            mediaItem.soundLevel = self.defaultAudioVolume
            
        }
        
        self.refreshData()
        
    }
    
}

// MARK: Extension for UI Actions
extension BassConverterViewController {
    
    func setupUIViews() {
        
        self.tableView.tableFooterView = UIView()
        let nibName = UINib(nibName: "MediaTableViewCell", bundle:nil)
        
        self.tableView.register(nibName, forCellReuseIdentifier: "mediaCell")
        self.tableView.estimatedRowHeight = 58
        
        self.convertButton.layer.cornerRadius = 15.0
        
    }
    
    func openFormatSheet(mediaTableViewCell: MediaTableViewCell) {
        
        let sheet = UIAlertController.init(title: "MEDIA CONVERTER", message: "- Please select an output format -", preferredStyle: UIAlertController.Style.actionSheet)
        
        sheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { _ in
            self.convertButtonShouldBeEnabled = false
        }))
        if self.selectedMediaType == .Audio {
            sheet.addAction(UIAlertAction(title: "AAC",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "AAC"
                                            self.refreshData()
            }))
            sheet.addAction(UIAlertAction(title: "WAV",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "WAV"
                                            self.refreshData()
            }))
            sheet.addAction(UIAlertAction(title: "FLAC",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "FLAC"
                                            self.refreshData()
            }))
            sheet.addAction(UIAlertAction(title: "OPUS",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "OPUS"
                                            self.refreshData()
            }))
            sheet.addAction(UIAlertAction(title: "CAF",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "CAF"
                                            self.refreshData()
            }))
            sheet.addAction(UIAlertAction(title: "WMA",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "WMA"
                                            self.refreshData()
            }))
            sheet.addAction(UIAlertAction(title: "AIFF",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "AIFF"
                                            self.refreshData()
            }))
            sheet.addAction(UIAlertAction(title: "MP3",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "MP3"
                                            self.refreshData()
            }))
            sheet.addAction(UIAlertAction(title: "OGG",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "OGG"
                                            self.refreshData()
            }))
            sheet.addAction(UIAlertAction(title: "M4A",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "M4A"
                                            self.refreshData()
            }))
            sheet.addAction(UIAlertAction(title: "ADX",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "ADX"
                                            self.refreshData()
            }))
        } else {
            
            
            sheet.addAction(UIAlertAction(title: "MOV",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "MOV"
                                            self.refreshData()
            }))

            sheet.addAction(UIAlertAction(title: "MP4",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "MP4"
                                            self.refreshData()
            }))
            
            sheet.addAction(UIAlertAction(title: "MPG",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "MPG"
                                            self.refreshData()
            }))

            sheet.addAction(UIAlertAction(title: "AVI",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "AVI"
                                            self.refreshData()
            }))
            sheet.addAction(UIAlertAction(title: "WMV",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "WMV"
                                            self.refreshData()
            }))
            sheet.addAction(UIAlertAction(title: "OGG",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "OGG"
                                            self.refreshData()
            }))
            sheet.addAction(UIAlertAction(title: "WEBM",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "WEBM"
                                            self.refreshData()
            }))
            sheet.addAction(UIAlertAction(title: "MKV (H.264)",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "MKV (H.264)"
                                            self.refreshData()
            }))
            sheet.addAction(UIAlertAction(title: "MKV (H.265, slow)",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "MKV (H.265, slow)"
                                            self.refreshData()
            }))
            sheet.addAction(UIAlertAction(title: "M2TS",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "M2TS"
                                            self.refreshData()
            }))
            sheet.addAction(UIAlertAction(title: "GIF",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "GIF"
                                            self.refreshData()
            }))
        }
        
        CommonClass.sharedInstance.showPopoverFrom(mediaTableViewCell: mediaTableViewCell, forButton: mediaTableViewCell.formatButton, sheet: sheet, owner: self, tableView: self.tableView)
        
    }
    
    func openAudioSheet(mediaTableViewCell: MediaTableViewCell) {
        
        let sheet = UIAlertController.init(title: "BASS BOOSTER", message: "- Please select a bass level -", preferredStyle: UIAlertController.Style.actionSheet)
        
        sheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { _ in
            self.convertButtonShouldBeEnabled = false
        }))

        sheet.addAction(UIAlertAction(title: "0 % (No Change)",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "0 %  (No Change)"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "10 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "10 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "20 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "20 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "30 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "30 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "40 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "40 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "50 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "50 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "60 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "60 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "70 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "70 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "80 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "80 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "90 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "90 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "100 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "100 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "110 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "110 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "120 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "120 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "130 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "130 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "140 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "140 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "150 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "150 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "160 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "160 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "170 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "170 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "180 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "180 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "190 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "190 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "200 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "200 %"
                                        self.refreshData()
        }))
        
        CommonClass.sharedInstance.showPopoverFrom(mediaTableViewCell: mediaTableViewCell, forButton: mediaTableViewCell.audioButton, sheet: sheet, owner: self, tableView: self.tableView)
        
    }
    
    func refreshData() {
        self.tableView.reloadData()
    }
    
}

// MARK: Extension for IBActions
extension BassConverterViewController {
    
    @IBAction func didTapDone(sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didTapConvert(sender: UIButton) {
        //SVProgressHUD.show()
        self.loadingView.isHidden = false
        self.batchConvertItemsInArray()
    }
    
    @IBAction func didTapCancel(sender: UIButton) {
        self.loadingView.isHidden = true
        MobileFFmpeg.cancel()
        self.ffmpegCommandsArray.removeAllObjects()
        self.destinationFilePathsArray.removeAllObjects()
    }
    
}

// MARK: Extension for Table View
extension BassConverterViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.mediaItemsArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = setupCell(indexPath: indexPath , tableView : tableView)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    @objc func setupCell(indexPath: IndexPath , tableView: UITableView) -> UITableViewCell {
        
        let model = self.mediaItemsArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "mediaCell", for: indexPath) as! MediaTableViewCell
        cell.bindData(model: model, searchTerm: "")
        
        cell.audioStackView.isHidden = false
        cell.formatStackView.isHidden = false
        cell.moreImageView.isHidden = true
        
        cell.formatButton.isHidden = false
        cell.audioButton.isHidden = false
        cell.formatButton.tag = indexPath.row
        cell.audioButton.tag = indexPath.row
        cell.formatButton.addTarget(self, action:#selector(formatButtonPressed(sender:)), for: .touchUpInside)
        cell.audioButton.addTarget(self, action:#selector(audioButtonPressed(sender:)), for: .touchUpInside)
        
        return cell        
    }
    
    @objc func audioButtonPressed(sender: UIButton) {
        
        let indexPath = IndexPath(row: sender.tag, section: 0)
        let mediaTableViewCell = tableView.cellForRow(at: indexPath) as! MediaTableViewCell
        convertButtonShouldBeEnabled = true
        self.selectedMediaItem = mediaItemsArray[sender.tag]
        
        self.openAudioSheet(mediaTableViewCell: mediaTableViewCell )
        
    }
    
    @objc func formatButtonPressed(sender: UIButton) {
        
        let indexPath = IndexPath(row: sender.tag, section: 0)
        let mediaTableViewCell = tableView.cellForRow(at: indexPath) as? MediaTableViewCell
        convertButtonShouldBeEnabled = true
        self.selectedMediaItem = mediaItemsArray[sender.tag]
        
        self.openFormatSheet(mediaTableViewCell: mediaTableViewCell ?? MediaTableViewCell())
        
    }

}

// MARK: Extension for Conversion
extension BassConverterViewController {
    
    // Audio Methods Start
    func convertToAudioFormat(ffmpegCommand: String, destinationFilePath: String/*, downloadGroup: DispatchGroup*/, mediaItem: MediaItems) {
        
        MobileFFmpegConfig.setLogDelegate(self)
        
        print("ffmpegCommand: \(ffmpegCommand)")
        
        let result = MobileFFmpeg.execute(ffmpegCommand)
        if result != RETURN_CODE_SUCCESS {
                   DispatchQueue.main.async {
                       self.loadingView.isHidden = true

                       print("stabilisation analyze process failed with result: \(result)")
                       self.ffmpegCommandsArray.removeAllObjects()
                       self.destinationFilePathsArray.removeAllObjects()
                       if result != RETURN_CODE_CANCEL {
                           CommonClass.sharedInstance.showAlertWithTitle(title: "Error", message: "Conversion process failed!", owner: self)
                       }
                       return
                   }
               }
        print("result: \(result)")
    }
    
    func exportFinalAudioToAppGallery(destinationFilePath: String, mediaItem: MediaItems) {
        
        guard let destinationFilePathURL  = URL.init(string: destinationFilePath) else { return }
        
        var duration = 0.0
        var fileSize = 0.0
        
        if ExportUtility.sharedInstance.isAudioSupportedByFFMPEG(destinationFilePath: destinationFilePath) {
            duration = CommonClass.sharedInstance.getDurationOfFileInSecondsUsingFFMPEG(url: destinationFilePathURL)
            fileSize = CommonClass.sharedInstance.getFileSizeFromURLInMB(url: destinationFilePathURL)
        }
        else {
            duration = CommonClass.sharedInstance.getDurationOfFileInSeconds(url: destinationFilePathURL)
            fileSize = CommonClass.sharedInstance.getFileSizeFromURLInMB(url: destinationFilePathURL)
        }
        
        // Get their filesize from a different method
        if((destinationFilePath.range(of: "opus")) != nil) {
            fileSize = CommonClass.sharedInstance.getFileSizeFromURLInMBForOggAndOpus(url: destinationFilePathURL as URL)
        }
        else if((destinationFilePath.range(of: "ogg")) != nil) {
            fileSize = CommonClass.sharedInstance.getFileSizeFromURLInMBForOggAndOpus(url: destinationFilePathURL as URL)
        }
        
        let audioModel = ImportUtility.sharedInstance.getAudioModelForID(id: mediaItem.id)
        let audioExportModel = AudioExportModel()
        audioExportModel.id = ExportUtility.sharedInstance.getIDForNewAudioExportModel()
        audioExportModel.size = fileSize
        audioExportModel.duration = duration
        audioExportModel.filePath = "\(destinationFilePathURL.lastPathComponent)"
        audioExportModel.fileType = mediaItem.fileTypeToConvert
        audioExportModel.dateCreated = CommonClass.sharedInstance.getCurrentDate()
        audioExportModel.soundLevel = mediaItem.soundLevel
        let fullModelName = "\(mediaItem.fullName) \(CommonClass.sharedInstance.getCurrentDateInString()) [\(mediaItem.soundLevel)]"

        let modelName = "\(audioModel.name) \(CommonClass.sharedInstance.getCurrentDateInString()) [Bass \(mediaItem.soundLevel)].\(mediaItem.fileTypeToConvert)"
        audioExportModel.name = modelName
        if mediaItem.fullName.count > 0 {
            audioExportModel.fullName = fullModelName
        }

        ExportUtility.sharedInstance.addAudioExportModelToDatabase(audioExportModel: audioExportModel)
        
    }
    
    // Audio Methods End
}

// MARK: Extension for Conversion
extension BassConverterViewController {
    
    // Video Methods Start
    func convertToVideoFormat(ffmpegCommand: String, destinationFilePath: String/*, downloadGroup: DispatchGroup*/, mediaItem: MediaItems) {
        
        MobileFFmpegConfig.setLogDelegate(self)
        
        print("ffmpegCommand: \(ffmpegCommand)")
        let result = MobileFFmpeg.execute(ffmpegCommand)
        
        if result != RETURN_CODE_SUCCESS {
            DispatchQueue.main.async {
                self.loadingView.isHidden = true

                print("stabilisation analyze process failed with result: \(result)")
                self.ffmpegCommandsArray.removeAllObjects()
                self.destinationFilePathsArray.removeAllObjects()
                if result != RETURN_CODE_CANCEL {
                    CommonClass.sharedInstance.showAlertWithTitle(title: "Error", message: "Conversion process failed!", owner: self)
                }
                return
            }
        }
        print("result: \(result)")
        
        
        
        
        
    }
    
    func exportFinalVideoToAppGallery(destinationFilePath: String, mediaItem: MediaItems) {
        
        guard let destinationFilePathURL  = URL.init(string: destinationFilePath) else { return }
        
        var duration = 0.0
        var fileSize = 0.0
        
        if ExportUtility.sharedInstance.isAudioSupportedByFFMPEG(destinationFilePath: destinationFilePath) {
            duration = CommonClass.sharedInstance.getDurationOfFileInSecondsUsingFFMPEG(url: destinationFilePathURL)
            fileSize = CommonClass.sharedInstance.getFileSizeFromURLInMB(url: destinationFilePathURL)
        }
        else {
            duration = CommonClass.sharedInstance.getDurationOfFileInSeconds(url: destinationFilePathURL)
            fileSize = CommonClass.sharedInstance.getFileSizeFromURLInMB(url: destinationFilePathURL)
        }
        
        // Get their filesize from a different method
        if((destinationFilePath.range(of: "ogg")) != nil) {
            fileSize = CommonClass.sharedInstance.getFileSizeFromURLInMBForOggAndOpus(url: destinationFilePathURL as URL)
        }
        
        let videoModel = ImportUtility.sharedInstance.getVideoModelForID(id: mediaItem.id)
        let videoExportModel = VideoExportModel()
        videoExportModel.id = ExportUtility.sharedInstance.getIDForNewVideoExportModel()
        videoExportModel.size = fileSize
        videoExportModel.duration = duration
        videoExportModel.filePath = "\(destinationFilePathURL.lastPathComponent)"
        videoExportModel.fileType = mediaItem.fileTypeToConvert
        videoExportModel.dateCreated = CommonClass.sharedInstance.getCurrentDate()
        videoExportModel.soundLevel = mediaItem.soundLevel
        
        var modelName = "\(videoModel.name) \(CommonClass.sharedInstance.getCurrentDateInString()) [\(mediaItem.soundLevel)]"
        var fullModelName = "\(mediaItem.fullName) \(CommonClass.sharedInstance.getCurrentDateInString()) [\(mediaItem.soundLevel)]"
        
        
        if stabilizeVideo == true {
            modelName = "\(videoModel.name) \(CommonClass.sharedInstance.getCurrentDateInString()) [\(mediaItem.soundLevel)]-stabilized"
            fullModelName = "\(mediaItem.fullName) \(CommonClass.sharedInstance.getCurrentDateInString()) [\(mediaItem.soundLevel)]-stabilized"

        }
        videoExportModel.name = modelName
        videoExportModel.fullName = fullModelName

        ExportUtility.sharedInstance.addVideoExportModelToDatabase(videoExportModel: videoExportModel)
        
    }
    
    // Video Methods End
}

// MARK: Extension for Batch Convert
extension BassConverterViewController {
    func batchConvertItemsInArray() {
        for mediaItem in self.mediaItemsArray {
            var sourceFilePath = ""
            var fileName = NSString()
            
            if self.selectedMediaType == .Audio {
                let audioModel = ImportUtility.sharedInstance.getAudioModelForID(id: mediaItem.id)
                sourceFilePath = audioModel.filePath
                fileName = audioModel.filePath as NSString
            } else {
                let model = ImportUtility.sharedInstance.getVideoModelForID(id: mediaItem.id)
                sourceFilePath = model.filePath
                fileName = model.filePath as NSString
            }
            
            var sourceFilePathURL = NSURL(string: sourceFilePath) ?? NSURL()
            
            let fileNameWithExt = sourceFilePath
            var sourceUrl = URL.init(string: "")
            
            if self.selectedMediaType == .Audio {
                sourceUrl = ImportUtility.sharedInstance.getDocumentsDirectoryWithAudioFolder().appendingPathComponent(fileNameWithExt)
            } else {
                sourceUrl = ImportUtility.sharedInstance.getDocumentsDirectoryWithVideoFolder().appendingPathComponent(fileNameWithExt)
            }
            
            sourceFilePathURL = sourceUrl! as NSURL
            
            if self.selectedMediaType == .Audio {
                let destinationFilePathURL = ExportUtility.sharedInstance.getDestinationFilePath(sourceFileUrl: sourceFilePathURL as URL, fileName: fileName.deletingPathExtension, fileExtension: mediaItem.fileTypeToConvert, volumeBoost: "bass\(mediaItem.soundLevel)")
                
                // Get ffmpeg command according to format
                let ffmpegCommand = ExportUtility.sharedInstance.getAudioBassConvertFFMPEGCommand(extention: mediaItem.fileTypeToConvert, volumeBoost: mediaItem.soundLevel, sourceFilePath: sourceFilePathURL.absoluteString ?? "", destinationFilePath: destinationFilePathURL.absoluteString)
                
                ffmpegCommandsArray.add(ffmpegCommand)
                destinationFilePathsArray.add(destinationFilePathURL)
            }
        }
        self.performFFMPEGConversion()
    }
    
    func performFFMPEGConversion() {
        
        var index = 0
        DispatchQueue.global(qos: .userInitiated).async {
            
            for _ in self.ffmpegCommandsArray {
                
                if index < self.ffmpegCommandsArray.count && index < self.destinationFilePathsArray.count && index < self.mediaItemsArray.count {
                    let ffmpegCommand = self.ffmpegCommandsArray[index] as? String
                    let destinationFilePathURL = self.destinationFilePathsArray[index] as? URL
                    let mediaItem = self.mediaItemsArray[index]
                    
                    if self.selectedMediaType == .Audio {
                        self.convertToAudioFormat(ffmpegCommand: ffmpegCommand ?? "", destinationFilePath: destinationFilePathURL?.absoluteString ?? "", mediaItem: mediaItem)
                    }
                    else {
                        self.convertToVideoFormat(ffmpegCommand: ffmpegCommand ?? "", destinationFilePath: destinationFilePathURL?.absoluteString ?? "", mediaItem: mediaItem)
                    }
                }
                
                index = index+1
                
            }
            
            if self.selectedMediaType == .Audio {
                DispatchQueue.main.async {
                    print("all done")
                    if self.loadingView.isHidden == true {
                        return
                    }
                    for i in 0..<self.mediaItemsArray.count {
                        // Export to Realm DB
                        self.exportFinalAudioToAppGallery(destinationFilePath: (self.destinationFilePathsArray[i] as? URL)?.absoluteString ?? "", mediaItem: self.mediaItemsArray[i])
                        // Update Gallery
                        NotificationCenter.default.post(name: Notification.Name("GalleryNeedsUpdate"), object: nil, userInfo: nil)
                    }
                    self.loadingView.isHidden = true

                    //SVProgressHUD.dismiss()
                    
                    let alert = UIAlertController(title: "Successfully BOOSTED!", message: "You can find the BOOSTED file in the Result tab.", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: { (action) in
                        self.dismiss(animated: true) {
                            self.delegate?.didConvertedAudio()
                        }
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else {
                self.performVideoStabilization()
            }
        }
    }
    
}

// MARK: Extension for Log Delegate
extension BassConverterViewController: LogDelegate {
    
    func logCallback(_ level: Int32, _ message: String!) {
        print("FFMPEG - message from FFMPEG: \(message ?? "")")
    }
    
}

// MARK: Video Work
extension BassConverterViewController {

    func openAppAudioVC() {
        let vc = AppAudioGalleryViewController(nibName: "AppAudioGalleryViewController", bundle: nil)
        
        vc.delegate = self
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
}

// MARK: Extension for Stabilisation
extension BassConverterViewController {
    
    func performVideoStabilization() {
        
        if(self.stabilizeVideo) {
            self.buildTransformFile()
        }
        else {
            DispatchQueue.main.async {
                print("all done")
                if self.loadingView.isHidden == true {
                    return
                    
                }
                for i in 0..<self.mediaItemsArray.count {
                    // Export to Realm DB
                    self.exportFinalVideoToAppGallery(destinationFilePath: (self.destinationFilePathsArray[i] as? URL)?.absoluteString ?? "", mediaItem: self.mediaItemsArray[i])
                    // Update Gallery
                    NotificationCenter.default.post(name: Notification.Name("GalleryNeedsUpdate"), object: nil, userInfo: nil)
                }
                self.loadingView.isHidden = true
                //SVProgressHUD.dismiss()
                
                let alert = UIAlertController(title: "Successfully BOOSTED!", message: "You can find the BOOSTED file in the Result tab.", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: { (action) in
                    self.dismiss(animated: true) {
                        self.delegate?.didConvertedAudio()
                    }
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
    }
    
    func buildTransformFile() {
        
        let videoFile = self.destinationFilePathsArray[0] as? URL
        let fileName = videoFile?.lastPathComponent.fileName()
        let pathExtension = videoFile?.pathExtension
        
        let shakeResultsFile = ExportUtility.sharedInstance.getTransformFilePath()
        let stabilizedVideoFile = ExportUtility.sharedInstance.getStabilizedVideoPath(fileName: fileName ?? "", pathExtension: pathExtension ?? "")
        
        DispatchQueue.main.async {
            //SVProgressHUD.dismiss()
            self.loadingView.isHidden = false

            print("done creating video, now moving to stabilising")
            
            let analyzeFFmpegCommand = "-hide_banner -y -i \(self.destinationFilePathsArray[0]) -vf vidstabdetect=stepsize=32:shakiness=10:accuracy=15:result=\(ExportUtility.sharedInstance.getTransformFilePath()) -f null -"
            
            //SVProgressHUD.show(withStatus: "Analyzing video for stabilisation")
            
            DispatchQueue.global(qos: .default).async {
                MobileFFmpegConfig.setLogDelegate(self)
                let result = MobileFFmpeg.execute(analyzeFFmpegCommand)
                
                print("stabilisation analyze process ended in result: \(result)")
                //SVProgressHUD.dismiss()

                DispatchQueue.main.async {
                    if result == RETURN_CODE_SUCCESS {
                        //SVProgressHUD.show(withStatus: "Stabilising video")
                        DispatchQueue.main.async {
                            self.loadingView.isHidden = false
                        }
                        DispatchQueue.global(qos: .default).async {
                            let stabiliseFFmpegCommand = "-hide_banner -y -i \(self.destinationFilePathsArray[0]) -vf vidstabtransform=smoothing=30:input=\(shakeResultsFile) \(stabilizedVideoFile)"
                            print("stabiliseFFmpegCommand: \(stabiliseFFmpegCommand)")
                            
                            let result = MobileFFmpeg.execute(stabiliseFFmpegCommand)
                            print("stabilisation transform process ended in result: \(result)")
                            
                            DispatchQueue.main.async {
                                if result == RETURN_CODE_SUCCESS {
                                    print("stabilisation transform process completed successfully")
                                    if self.loadingView.isHidden == false {
                                        self.exportStabilisedVideoToAppGallery(stabilizedVideoFile: stabilizedVideoFile)
                                    }
                                }
                                else {
                                    //SVProgressHUD.dismiss()
                                    self.ffmpegCommandsArray.removeAllObjects()
                                    self.destinationFilePathsArray.removeAllObjects()
                                    self.loadingView.isHidden = true
                                    if result != RETURN_CODE_CANCEL {
                                        CommonClass.sharedInstance.showAlertWithTitle(title: "Error", message: "Stabilisation transform process failed!", owner: self)
                                    }
                                    print("stabilisation transform process failed with result: \(result)")
                                }
                            }
                        }
                    }
                    else {
                        //SVProgressHUD.dismiss()
                        self.ffmpegCommandsArray.removeAllObjects()
                        self.destinationFilePathsArray.removeAllObjects()
                        self.loadingView.isHidden = true
                        if result != RETURN_CODE_CANCEL {
                            CommonClass.sharedInstance.showAlertWithTitle(title: "Error", message: "Stabilisation analyze process failed!", owner: self)
                        }

                        print("stabilisation analyze process failed with result: \(result)")
                    
                    }
                }
            }
        }
        
    }
    
    func exportStabilisedVideoToAppGallery(stabilizedVideoFile: URL) {
        DispatchQueue.main.async {
            // Export to Realm DB
            self.exportFinalVideoToAppGallery(destinationFilePath: stabilizedVideoFile.absoluteString, mediaItem: self.mediaItemsArray[0])
            // Update Gallery
            NotificationCenter.default.post(name: Notification.Name("GalleryNeedsUpdate"), object: nil, userInfo: nil)
            //SVProgressHUD.dismiss()
            self.loadingView.isHidden = true
            
            let alert = UIAlertController(title: "Successfully BOOSTED!", message: "You can find the BOOSTED file in the Result tab.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: { (action) in
                self.dismiss(animated: true) {
                    self.delegate?.didConvertedAudio()
                }
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
}

extension BassConverterViewController: AppAudioGalleryViewControllerDelegate {
    func didSelectAudio(withMediaItems mediaItems: MediaItems) {
       
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
       let audioModel = ExportUtility.sharedInstance.getAudioExportModelForID(id: mediaItems.id)
        var songTitle = audioModel.name
        var filePath = audioModel.filePath
        var url = ExportUtility.sharedInstance.getDocumentsDirectoryWithAudioFolder().appendingPathComponent(filePath)

        
        if audioModel.id == 0 {
            let audioModel = ImportUtility.sharedInstance.getAudioModelForID(id: mediaItems.id)
            songTitle = audioModel.name
            filePath = audioModel.filePath
            url = ImportUtility.sharedInstance.getDocumentsDirectoryWithAudioFolder().appendingPathComponent(filePath)
        }
        
        self.dismiss(animated: true) {
            self.musicURL = url
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
            }
        }
    }
}
