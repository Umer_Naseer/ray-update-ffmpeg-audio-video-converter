//
//  SettingsViewController.swift
//  AV enhance
//
//  Created by umer on 8/3/19.
//  Copyright © 2019 umer. All rights reserved.
//

import UIKit
import StoreKit

class SettingsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var previewBeforeImporting = "false"
    
    var settingsArray = [SettingsModel]()

    override func viewDidLoad() {
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if let previewBeforeImporting = UserDefaults.standard.value(forKey: Constants.UserDefaults.k_Preview_Before_Importing) as? String {
            self.previewBeforeImporting = previewBeforeImporting
        }
        
        self.mapDataSource()
        self.setupUIViews()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tableView.reloadData()
    }

}

// Private Methods
extension SettingsViewController {
    
    func setupUIViews() {
        
        self.tableView.tableFooterView = UIView()
        let nibName = UINib(nibName: "SettingsTableViewCell", bundle:nil)
        
        self.tableView.register(nibName, forCellReuseIdentifier: "settingsCell")
        self.tableView.estimatedRowHeight = 58
        
    }
    
    func mapDataSource() {
        
        self.settingsArray = [SettingsModel]()
        for i in 0...3 {
            let settings = SettingsModel()
            switch i {
            case 0:
                settings.type.append(SettingsRowType.AudioVolume)
                settings.titleArray.append("Default Volume")
                settings.titleArray.append("Preview Before Importing")
                settings.type.append(SettingsRowType.PreviewBeforeImporting)
            case 1:
                settings.type.append(SettingsRowType.VideoFormat)
                settings.titleArray.append("Default Video Format")
                settings.type.append(SettingsRowType.AudioFormat)
                settings.titleArray.append("Default Audio Format")
            case 2:
                settings.type.append(SettingsRowType.ShareOurApp)
                settings.type.append(SettingsRowType.RateNReview)
                settings.type.append(SettingsRowType.FeedBack)
                settings.titleArray.append("Share Our App")
                settings.titleArray.append("Rate & Review Our App")
                settings.titleArray.append("Help & Support")
            case 3:
                settings.type.append(SettingsRowType.PrivacyPolicy)
                settings.type.append(SettingsRowType.TermNCondition)
                settings.titleArray.append("Privacy Policy")
                settings.titleArray.append("Terms")
            default:
                print("")
            }
            self.settingsArray.append(settings)
        }
        self.tableView.reloadData()
        
    }
    
    func openActivityIndicator(settingsTableViewCell: SettingsTableViewCell) {
        if let urlStr = NSURL(string: appLink) {
            let objectsToShare = [urlStr]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)

            if UIDevice.current.userInterfaceIdiom == .pad {
                if let popup = activityVC.popoverPresentationController {
                    popup.sourceView = self.view
                    popup.sourceRect = CGRect(x: self.view.frame.size.width / 2, y: self.view.frame.size.height / 4, width: 0, height: 0)
                }
            }

            self.showPopoverFrom(settingsTableViewCell: settingsTableViewCell, label: settingsTableViewCell.titleLabel, sheet: activityVC)

        }
        
    }
    
    func rateApp(){
        if #available(iOS 10.3, *) {
            //SKStoreReviewController.requestReview()
            if #available(iOS 14.0, *) {
                    if let scene = UIApplication.shared.connectedScenes.first(where: { $0.activationState == .foregroundActive }) as? UIWindowScene {
                            SKStoreReviewController.requestReview(in: scene)
                    }
            } else if #available(iOS 10.3, *) {
                    SKStoreReviewController.requestReview()
            }

        } else if let url = URL(string: appLink) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)

            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func showPopoverFrom(settingsTableViewCell: SettingsTableViewCell, label: UILabel, sheet: UIActivityViewController) {
        
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            self.present(sheet, animated: true, completion: nil)
        case .pad:
            /* Get the souce rect frame */
            let buttonFrame = label.frame
            var showRect    = settingsTableViewCell.convert(buttonFrame, to: self.tableView)
            showRect        = self.tableView.convert(showRect, to: self.view)
            showRect.origin.y -= 10
            
            if let popoverPresentationController = sheet.popoverPresentationController {
                popoverPresentationController.permittedArrowDirections = .up
                popoverPresentationController.sourceView = self.view
                popoverPresentationController.sourceRect = showRect
                self.present(sheet, animated: true, completion: nil)
            }
        case .unspecified:
            self.present(sheet, animated: true, completion: nil)
        default:
            print("")
        }
        
    }
    
}

extension SettingsViewController: UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
            return 45
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return self.settingsArray.count
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.settingsArray[section].titleArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell", for: indexPath) as! SettingsTableViewCell
        
        cell.settingsSwitch.isHidden = true
        cell.iconImageView.isHidden = false
        
        cell.iconImageView.image = UIImage(named: "next_icon")
        cell.titleLabel.text = self.settingsArray[indexPath.section].titleArray[indexPath.row]
        if self.settingsArray.count > indexPath.section {
            if self.settingsArray[indexPath.section].valueArray.count > indexPath.row {
                cell.valueLabel.text = self.settingsArray[indexPath.section].valueArray[indexPath.row]
            } else {
                cell.valueLabel.text = ""
                switch self.settingsArray[indexPath.section].type[indexPath.row] {
                case .AudioFormat:
                    if let defaultAudioFormat = UserDefaults.standard.value(forKey: Constants.UserDefaults.k_Default_Audio_Format) as? String {
                        cell.valueLabel.text = "\(defaultAudioFormat)"
                    }
                    else{
                        cell.valueLabel.text = "AAC"
                    }
                case .VideoFormat:
                    if let defaultVideoFormat = UserDefaults.standard.value(forKey: Constants.UserDefaults.k_Default_Video_Format) as? String {
                        cell.valueLabel.text = "\(defaultVideoFormat)"
                    }
                    else{
                        cell.valueLabel.text = "MOV"
                    }
                    
                case .AudioVolume:
                    if let defaultAudioVolume = UserDefaults.standard.value(forKey: Constants.UserDefaults.k_Default_Audio_Volume) as? String {
                        cell.valueLabel.text = "\(defaultAudioVolume)"
                    }
                    else{
                        cell.valueLabel.text = "100% (No Change)"
                    }
                    
                case .PreviewBeforeImporting:
                    cell.settingsSwitch.isHidden = false
                    cell.iconImageView.isHidden = true
                    
                    if(self.previewBeforeImporting == "true") {
                        cell.settingsSwitch?.setOn(true, animated: true)
                    }
                    else {
                        cell.settingsSwitch?.setOn(false, animated: true)
                    }
                    
                    cell.settingsSwitch?.addTarget(self, action: #selector(settingSwitchValueChanged(sender:)), for: .valueChanged)
                default:
                    print("")
                }
            }
        } else {
            cell.valueLabel.text = ""
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let settingsTableViewCell = tableView.cellForRow(at: indexPath) as? SettingsTableViewCell
        let type = self.settingsArray[indexPath.section].type[indexPath.row]
        
        switch type {
        case .AudioVolume:
            let vc = AudioNVideoFormatViewController(nibName: "AudioNVideoFormatViewController", bundle: nil)
            vc.selectedScreenType = type
            self.navigationController?.pushViewController(vc, animated: true)
        case .VideoFormat:
            let vc = AudioNVideoFormatViewController(nibName: "AudioNVideoFormatViewController", bundle: nil)
            vc.selectedScreenType = type
            self.navigationController?.pushViewController(vc, animated: true)
        case .AudioFormat:
            let vc = AudioNVideoFormatViewController(nibName: "AudioNVideoFormatViewController", bundle: nil)
            vc.selectedScreenType = type
            self.navigationController?.pushViewController(vc, animated: true)
        case .RemoveAdd:
            print("")
        case .Restore:
            print("")
        case .ShareOurApp:
            self.openActivityIndicator(settingsTableViewCell: settingsTableViewCell ?? SettingsTableViewCell())
        case .RateNReview:
            self.rateApp()
        case .FeedBack:
            CommonClass.sharedInstance.openUrlInSafari(urlString: "http://www.9bestapp.com/booster/index.html")
        case .MoreApps:
            print("")
        case .PrivacyPolicy:
            CommonClass.sharedInstance.openUrlInSafari(urlString: "http://www.9bestapp.com/resources/privacy-policy.html")
        case .TermNCondition:
            CommonClass.sharedInstance.openUrlInSafari(urlString: "http://www.9bestapp.com/terms/")
        case .ItunesFileSharing:
            print("")
        case .PreviewBeforeImporting:
            print("")
        }
        
    }
    
}

extension SettingsViewController {
    
    @objc func settingSwitchValueChanged(sender: UISwitch) {
        
        if(sender.isOn) {
            self.previewBeforeImporting = "true"
        }
        else {
            self.previewBeforeImporting = "false"
        }
        
        UserDefaults.standard.set(self.previewBeforeImporting, forKey: Constants.UserDefaults.k_Preview_Before_Importing)
        
    }
    
}
