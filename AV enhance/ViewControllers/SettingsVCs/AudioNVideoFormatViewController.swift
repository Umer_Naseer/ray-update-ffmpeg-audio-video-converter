//
//  AudioNVideoFormatViewController.swift
//  AV enhance
//
//  Created by umer on 8/4/19.
//  Copyright © 2019 umer. All rights reserved.
//

import UIKit

class AudioNVideoFormatViewController: UIViewController {
    
    // IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    // Class Variables
    var searchTerm = String()
    var selectedScreenType = SettingsRowType.AudioVolume
    
    var defaultAudioVolume = "100% (No Change)"
    var defaultVideoFormat = "mov"
    var defaultAudioFormat = "aac"
    
    var datasourceArray = [String]()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        // Default Audio Volume
        if let defaultAudioVolume = UserDefaults.standard.value(forKey: Constants.UserDefaults.k_Default_Audio_Volume) as? String {
            self.defaultAudioVolume = defaultAudioVolume
        }
        
        // Default Video Format
        if let defaultVideoFormat = UserDefaults.standard.value(forKey: Constants.UserDefaults.k_Default_Video_Format) as? String {
            self.defaultVideoFormat = defaultVideoFormat
        }
        
        // Default Audio Format
        if let defaultAudioFormat = UserDefaults.standard.value(forKey: Constants.UserDefaults.k_Default_Audio_Format) as? String {
            self.defaultAudioFormat = defaultAudioFormat
        }
        
        self.mapDataSource()
        self.setupUIViews()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        if let tabbar = self.tabBarController {
            CommonClass.sharedInstance.setTabBarTitles(tabbarVC: tabbar)
        }
        
    }
}

// Private Methods
extension AudioNVideoFormatViewController {
    
    func setupUIViews() {
        
        self.tableView.tableFooterView = UIView()
        let nibName = UINib(nibName: "SettingsTableViewCell", bundle:nil)
        
        self.tableView.register(nibName, forCellReuseIdentifier: "settingsCell")
        self.tableView.estimatedRowHeight = 58
        
    }
    
    func mapDataSource() {
        
        switch self.selectedScreenType {
        case .AudioVolume:
            self.datasourceArray.append("0.0 %(Mute)")
            self.datasourceArray.append("0.1 %")
            self.datasourceArray.append("0.5 %")
            self.datasourceArray.append("10.0 %")
            self.datasourceArray.append("20.0 %")
            self.datasourceArray.append("30.0 %")
            self.datasourceArray.append("40.0 %")
            self.datasourceArray.append("50.0 %")
            self.datasourceArray.append("60.0 %")
            self.datasourceArray.append("70.0 %")
            self.datasourceArray.append("80.0 %")
            self.datasourceArray.append("90.0 %")
            self.datasourceArray.append("100% (No Change)")
            self.datasourceArray.append("200.0 %")
            self.datasourceArray.append("300.0 %")
            self.datasourceArray.append("400.0 %")
            self.datasourceArray.append("500.0 %")
            self.datasourceArray.append("600.0 %")
            self.datasourceArray.append("700.0 %")
            self.datasourceArray.append("800.0 %")
            self.datasourceArray.append("900.0 %")
            self.datasourceArray.append("1000.0 %")
            self.titleLabel.text = "Audio Volume"
        case .VideoFormat:
            self.datasourceArray.append("MOV")
            self.datasourceArray.append("MP4")
            self.datasourceArray.append("MPG")
            self.datasourceArray.append("AVI")
            self.datasourceArray.append("WMV")
            self.datasourceArray.append("OGG")
            self.datasourceArray.append("WEBM")
            self.datasourceArray.append("MKV (H.264)")
            self.datasourceArray.append("MKV (H.265, slow)")
            self.datasourceArray.append("M2TS")
            self.datasourceArray.append("GIF")

            self.titleLabel.text = "Video Format"
        case .AudioFormat:
            self.datasourceArray.append("AAC")
            self.datasourceArray.append("WAV")
            self.datasourceArray.append("FLAC")
            self.datasourceArray.append("OPUS")
            self.datasourceArray.append("CAF")
            self.datasourceArray.append("WMA")
            self.datasourceArray.append("AIFF")
            self.datasourceArray.append("MP3")
            self.datasourceArray.append("OGG")
            self.datasourceArray.append("M4A")
            self.datasourceArray.append("ADX")
            self.titleLabel.text = "Audio Format"
        default:
            print("")
        }
        self.tableView.reloadData()
        
    }
}

extension AudioNVideoFormatViewController {
    
    @IBAction func didTapBack(sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
}

extension AudioNVideoFormatViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 45
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.datasourceArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = setupCell(indexPath: indexPath , tableView : tableView)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch self.selectedScreenType {
        case .AudioVolume:
            let volume = self.datasourceArray[indexPath.row]
            self.defaultAudioVolume = volume
            UserDefaults.standard.set(self.defaultAudioVolume, forKey: Constants.UserDefaults.k_Default_Audio_Volume)
        case .VideoFormat:
            let format = self.datasourceArray[indexPath.row]
            self.defaultVideoFormat = format
            UserDefaults.standard.set(self.defaultVideoFormat, forKey: Constants.UserDefaults.k_Default_Video_Format)
        case .AudioFormat:
            let format = self.datasourceArray[indexPath.row]
            self.defaultAudioFormat = format
            UserDefaults.standard.set(self.defaultAudioFormat, forKey: Constants.UserDefaults.k_Default_Audio_Format)
        default:
            print("")
        }
        
        tableView.reloadData()
        
    }
    
    @objc func setupCell(indexPath: IndexPath , tableView: UITableView) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell", for: indexPath) as! SettingsTableViewCell
        
        cell.valueLabel.isHidden = true
        cell.titleLabel.text = self.datasourceArray[indexPath.row]
        cell.iconImageView.image = UIImage(named: "check_icon")
        
        switch self.selectedScreenType {
        case .AudioVolume:
            let volume = self.datasourceArray[indexPath.row]
            if(self.defaultAudioVolume == volume) {
                cell.iconImageView.isHidden = false
            }
            else {
                cell.iconImageView.isHidden = true
            }
        case .VideoFormat:
            let format = self.datasourceArray[indexPath.row]
            if(self.defaultVideoFormat == format) {
                cell.iconImageView.isHidden = false
            }
            else {
                cell.iconImageView.isHidden = true
            }
        case .AudioFormat:
            let format = self.datasourceArray[indexPath.row]
            if(self.defaultAudioFormat == format) {
                cell.iconImageView.isHidden = false
            }
            else {
                cell.iconImageView.isHidden = true
            }
        default:
            print("")
        }
        
        return cell
        
    }
}

